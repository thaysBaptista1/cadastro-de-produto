﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BussinessLayer.Facade;
using BussinessLayer.DTO;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestCategoria
    {

        CategoriaFacade cf = new CategoriaFacade();

        [TestMethod]
        public void TestMethod1()
        {
            using (CategoriaFacade facade = new CategoriaFacade())
            {               
                facade.SalvarCategoria("aaaa"); 
            }
        }
    }
}
