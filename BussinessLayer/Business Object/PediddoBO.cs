﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLayer.Dao;
using BussinessLayer.DTO;
using BussinessLayer.Model;

namespace BussinessLayer.Business_Object
{
    internal class PediddoBO : BaseBO
    {
        public PediddoBO(SqlServerDao dao) : base(dao)
        {
        }

        public void SalvarPedido(PedidoDTO pedido)
        {
            var tabela = AutoMapper.Mapper.Map<Pedido>(pedido);

            Dao.Inserir(tabela);
        }
    }
}
