﻿using AutoMapper;
using BussinessLayer.Dao;
using BussinessLayer.DTO;
using BussinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Business_Object
{
    internal class BaseBO
    {
        protected SqlServerDao Dao { get; set; }

        protected BaseBO(SqlServerDao dao)
        {
            Dao = dao;
        }

        static BaseBO()
        {
            //Inicializar automapper
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Pedido, PedidoDTO>()
                    .ForMember(dest => dest.Data, opt => opt.MapFrom(source => source.Data))
                    .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(source => source.ValorTotal ));

                cfg.CreateMap<PedidoDTO, Pedido>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(source => source.Id))
                    .ForMember(dest => dest.ValorTotal, opt => opt.MapFrom(source => source.ValorTotal))
                    .ForMember(dest => dest.Data, opt => opt.MapFrom(source => source.Data));

                cfg.CreateMap<Categoria, CategoriaDTO>()
                .ForMember(dest => dest.Descricao, opt => opt.MapFrom(source => source.Descricao));

                cfg.CreateMap<CategoriaDTO, Categoria>()
                .ForMember(dest => dest.Descricao, opt => opt.MapFrom(source => source.Descricao));
            });
        }
    }
}
