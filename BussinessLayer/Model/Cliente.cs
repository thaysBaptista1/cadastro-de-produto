﻿using Produto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Model
{
    internal class Cliente : Base
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public int Cnpj { get; set; }
        public virtual List<Pedido> Pedidos { get; set; }
    }
}
