﻿using Produto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Model
{
    internal class ItemPedido : Base
    {
        [Required]
        public int PrecoUnitario { get; set; }
        [Required]
        public int Quantidade { get; set; }
        [Required]
        public int PrecoTotal { get; set; }
        public Pedido pedido { get; set; }
        public virtual Produtoo produto { get; set; }
    }
}
