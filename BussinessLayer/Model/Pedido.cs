﻿using Produto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Model
{
    internal class Pedido : Base
    {
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public int ValorTotal { get; set; }
        public Cliente cliente { get; set; }
        public virtual List<ItemPedido> ItemPedidos { get; set; }
    }
}
