﻿using Produto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Model
{
    internal class Categoria : Base
    {

        [Required]
        public string Descricao { get; set; }
        public virtual List<Produtoo> Produtos { get; set; }
    }
}
