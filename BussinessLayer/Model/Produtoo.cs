﻿using Produto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Model
{
    internal class Produtoo : Base
    {
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Descricao { get; set; }
        [Required]
        public int Preco { get; set; }

        public  Categoria categoria { get; set; }
        public virtual List<ItemPedido> ItemPedidos { get; set; }
    }
}
