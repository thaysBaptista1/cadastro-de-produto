﻿using BussinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Dao
{
    internal class SqlServerDao : Produto.Dao.SqlServerDao
    {
        protected DbSet<Produtoo> DbSetProduto { get; set; }
        protected DbSet<Categoria> DbSetCategoria{ get; set; }
        protected DbSet<Cliente> DbSetCliente { get; set; }
        protected DbSet<Pedido> DbSetPedido { get; set; }
        protected DbSet<ItemPedido> DbSetItemPedido{ get; set; }
    }
}
