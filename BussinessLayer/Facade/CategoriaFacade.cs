﻿using BussinessLayer.Business_Object;
using BussinessLayer.Dao;
using BussinessLayer.DTO;
using BussinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Facade
{
    public class CategoriaFacade : IDisposable
    {

        private SqlServerDao dao;
        private CategoriaBO categoria;

        public CategoriaFacade()
        {
            this.dao = new SqlServerDao();
            this.categoria = new CategoriaBO(dao);
        }

        public void SalvarCategoria(String descricao) {
            var cat = new CategoriaDTO();

            cat.Descricao = descricao;

            categoria.SalvarCategoria(cat);

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dao != null) dao.Dispose();
            }
        }
    }
}
