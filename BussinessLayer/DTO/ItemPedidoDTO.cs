﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.DTO
{
    public class ItemPedidoDTO
    {
        public int Id { get; set; }
        public int PrecoUnitario { get; set; }
        public int Quantidade { get; set; }
        public int PrecoTotal { get; set; }
    }
}
